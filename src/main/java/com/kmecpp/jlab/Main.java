package com.kmecpp.jlab;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.kmecpp.jlib.console.Command;
import com.kmecpp.jlib.console.Console;

public class Main {

	public static void main(String[] args) {
		registerCommands();
		Console.start();
	}

	public static void registerCommands() {
		new TestCommand(new String[] { "test" }).register();
		Console.registerCommand("range", new String[] { "<first>", "<second>" }, "Prints a range of numbers", (args) -> {
			System.out.println(IntStream.range(Integer.parseInt(args[0]), Integer.parseInt(args[1]))
					.mapToObj(String::valueOf)
					.collect(Collectors.joining(", ")));
		});

	}

	public static class TestCommand extends Command {

		public TestCommand(String[] aliases) {
			super(aliases);
			setArgs(new String[] { "<number>" });
			setDescription("This is a test command");
		}

		@Override
		public void execute(String label, String[] args) {
			System.out.println("Test executed successfully!");
		}

	}

}
