package com.kmecpp.jlab;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Tester {

	/**
	 * Calls the constructor of the class with the given parameters
	 * 
	 * @param cls
	 *            the class to construct
	 * @param params
	 *            the parameters for the constructor
	 */
	public static void construct(Class<?> cls, Object... params) {
		Class<?>[] paramTypes = Arrays.stream(params).map((param) -> param.getClass()).collect(Collectors.toList()).toArray(new Class[0]);
		try {
			for (Constructor<?> constructor : cls.getDeclaredConstructors()) {
				if (paramsEqual(constructor.getParameterTypes(), paramTypes)) {
					System.out.println("new " + cls.getSimpleName()
							+ "(" + Arrays.stream(params).map(Object::toString).collect(Collectors.joining(", "))
							+ "): \"" + constructor.newInstance((Object[]) params) + "\"");
					return;
				}
			}
			throw new RuntimeException("Constructor not found! " + cls.getSimpleName() + "(" + Arrays.stream(paramTypes).map((type) -> type.getSimpleName()).collect(Collectors.joining(", ")) + ")");
		} catch (SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Invokes a method on the object
	 * 
	 * @param target
	 *            the target on which to invoke the method
	 * @param method
	 *            the method to call, as a string
	 * @param params
	 *            the method parameters
	 */
	public static void invoke(Object target, String method, Object... params) {
		Class<?> cls = target instanceof Class ? (Class<?>) target : target.getClass();
		//		boolean exact = method.contains("("); //Whether or not the parameters were specified
		String methodName = method; //exact ? method.substring(0, method.indexOf("(")) : method;

		//		String[] paramTypes = exact
		//				? Arrays.stream(method.substring(method.indexOf("(") + 1, method.indexOf(")")).split(","))
		//						.map(String::trim)
		//						.filter((p) -> !p.isEmpty())
		//						.toArray((size) -> new String[size])
		//				: null;

		try {
			Method targetMethod = null;
			for (Method m : cls.getDeclaredMethods()) {
				if (m.getName().equals(methodName)) {
					if (params.length == m.getParameterCount() && typesEqual(m.getParameterTypes(), getTypes(params))) {
						targetMethod = m;
						break;
					}
				}
			}
			if (targetMethod != null) {
				System.out.println((target instanceof Class ? ((Class<?>) target).getSimpleName() : "(" + target + ")") + "."
						+ methodName + "(" + Arrays.stream(params).map(Tester::toString).collect(Collectors.joining(", ")) + ")"
						+ ": " + toString(targetMethod.invoke(target, (Object[]) params)));
				return;
			} else {
				throw new RuntimeException("Method '" + methodName + "' does not exist!");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		//		throw new RuntimeException("Method '" + method + "' does not exist!");
	}

	public static void invoke(String method, Object... params) {
		try {
			Class<?> cls = Class.forName(Thread.currentThread().getStackTrace()[2].getClassName());
			//			if (Modifier.isStatic(cls.getDeclaredMethod(method, getTypes(params)).getModifiers())) {
			invoke(cls, method, params);
			//			} else {
			//				throw new IllegalArgumentException("Method is not static");
			//			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	//Tests if parameters are equal
	private static boolean paramsEqual(Class<?>[] first, Class<?>[] second) {
		if (first.length == second.length) {
			for (int i = 0; i < first.length; i++) {
				if (typesEqual(first[i], second[i])) {
					continue;
				}
				return false;
			}
			return true;
		}
		return false;
	}

	//Tests if the classes are equal
	private static boolean typesEqual(Class<?> t1, Class<?> t2) {
		return (t1.equals(t2)) || (typeMap.get(t1) != null && typeMap.get(t1).equals(typeMap.get(t2)));
	}

	private static boolean typesEqual(Class<?>[] arr1, Class<?>[] arr2) {
		if (arr1.length != arr2.length) {
			return false;
		}
		for (int i = 0; i < arr1.length; i++) {
			if (!typesEqual(arr1[i], arr2[i])) {
				return false;
			}
		}
		return true;
	}

	@SuppressWarnings("serial")
	private static final Map<Class<?>, Class<?>> typeMap = new HashMap<Class<?>, Class<?>>(16) {

		{
			put(boolean.class, Boolean.class);
			put(byte.class, Byte.class);
			put(char.class, Character.class);
			put(double.class, Double.class);
			put(float.class, Float.class);
			put(int.class, Integer.class);
			put(long.class, Long.class);
			put(short.class, Short.class);
			put(void.class, Void.class);
			put(Boolean.class, Boolean.class);
			put(Byte.class, Byte.class);
			put(Character.class, Character.class);
			put(Double.class, Double.class);
			put(Float.class, Float.class);
			put(Integer.class, Integer.class);
			put(Long.class, Long.class);
			put(Short.class, Short.class);
			put(Void.class, Void.class);
		}

	};

	public static Class<?> evalType(Object obj) {
		Class<?> cls = obj.getClass();
		if (cls.isArray()) {
			while (cls.isArray()) {
				cls = cls.getComponentType();
			}
		}
		Class<?> mapped = typeMap.get(obj.getClass());
		return mapped == null ? cls : mapped;
	}

	public static String toString(Object obj) {
		if (obj.getClass().isArray()) {
			return arrayToString(obj);
			//			if (evalType(obj).isPrimitive()) {
			//				
			//			} else {
			//				return Arrays.deepToString((Object[]) obj);
			//			}
		}
		return String.valueOf(obj);
	}

	public static String arrayToString(Object obj) {
		StringBuilder sb = new StringBuilder("[");
		for (int i = 0; i < Array.getLength(obj); i++) {
			Object element = Array.get(obj, i);
			sb.append((i == 0 ? "" : ", ") + (element.getClass().isArray() ? arrayToString(element) : Array.get(obj, i)));
		}
		return sb.append("]").toString();
	}

	public static Class<?>[] getTypes(Object[] arr) {
		Class<?>[] types = new Class<?>[arr.length];
		for (int i = 0; i < arr.length; i++) {
			types[i] = arr[i].getClass();
		}
		return types;
	}

	/**
	 * Prints the message to the console
	 * 
	 * @param message
	 *            the message to print
	 */
	public static void print(Object message) {
		System.out.println(message);
	}

	/**
	 * Prints a line separator to the console
	 */
	public static void line() {
		System.out.println("----------------------------------");
	}

}
